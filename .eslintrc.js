const ERR = 2;
const WARN = 1;
const NONE = 0;

module.exports = {
  extends: [
    'airbnb-base'
  ],
  rules: {
    'no-mixed-operators': NONE,
    'object-curly-newline': NONE,
    'import/prefer-default-export': WARN,
    'no-unused-vars': WARN,
    curly: ['error', 'multi', 'consistent']
  },
  parser: 'babel-eslint',
	overrides: [
	  Object.assign(
	    {
	      files: [ '**/*.test.js' ],
	      env: { jest: true },
	      plugins: [ 'jest' ],
	    },
	    require('eslint-plugin-jest').configs.recommended
	  )
	]
};
