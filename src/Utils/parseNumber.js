const parseNumber = (number) => {
  if (typeof number === 'number') return number;
  else if (typeof number === 'string') return Number(number.replace(/[^0123456789]/gi, ''));
  return null;
};

export default parseNumber;
