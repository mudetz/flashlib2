const zeroPad = (n, str) => '0'.repeat(n).concat(str).slice(-n);
export default zeroPad;
