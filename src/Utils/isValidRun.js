const dv = (X) => {
  let T = Number(X);
  let M = 0;
  let S = 1;
  for (; T; T = Math.floor(T / 10)) {
    S = (S + T % 10 * (9 - M % 6)) % 11;
    M += 1;
  }
  return S ? (S - 1).toString() : 'K';
};

const isValidRun = (run) => {
  if (!run || typeof run !== 'string') return false;
  if (!(/^\d{7,}(?:\d|K)$/).test(run)) return false;
  return run.slice(-1) === dv(run.slice(0, -1));
};

export default isValidRun;
