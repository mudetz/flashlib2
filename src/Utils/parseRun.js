const parseRun = (run) => {
  if (typeof run === 'number') return run.toString();
  else if (typeof run === 'string') return run.replace(/[^0123456789K]/g, '').toUpperCase();
  return null;
};

export default parseRun;
