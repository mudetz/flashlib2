const dv = (X) => {
  let T = X;
  let M = 0;
  let S = 1;
  for (; T; T = Math.floor(T / 10)) {
    S = (S + T % 10 * (9 - M % 6)) % 11;
    M += 1;
  }
  return S ? (S - 1).toString() : 'K';
};

export default dv;
