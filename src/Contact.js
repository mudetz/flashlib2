import { isValidRun, parseRun, parseNumber } from './Utils';
import { BANK, ACCOUNT } from './types';

const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default class Contact {
  constructor({ run, account, bank, type, name, email }) {
    if (!isValidRun(parseRun(run))) throw new Error(`invalid run ${run}`);
    if (!account || typeof account !== 'number') throw new Error('invalid account');
    if (!bank || !Object.values(BANK).includes(bank)) throw new Error('unsupported bank');
    if (!type || !Object.values(ACCOUNT).includes(type)) throw new Error('invalid account type');
    if (!name || typeof name !== 'string') throw new Error('invalid name');
    if (email && !emailRegex.test(email)) throw new Error('invalid email');

    this.run = parseRun(run);
    this.account = parseNumber(account);
    this.bank = bank;
    this.type = type;
    this.name = name;
    if (email) this.email = email;
  }
}
