export const BANK = Object.freeze({
  BANEFE: Symbol.for('BANEFE'),
  BBVA: Symbol.for('BBVA'),
  BCI: Symbol.for('BCI'),
  BCINOVA: Symbol.for('BCINOVA'),
  BICE: Symbol.for('BICE'),
  CHILE: Symbol.for('CHILE'),
  CONDELL: Symbol.for('CONDELL'),
  CORPBANCA: Symbol.for('CORPBANCA'),
  ESTADO: Symbol.for('ESTADO'),
  FALABELLA: Symbol.for('FALABELLA'),
  INTERNACIONAL: Symbol.for('INTERNACIONAL'),
  ITAU: Symbol.for('ITAU'),
  RIPLEY: Symbol.for('RIPLEY'),
  SANTANDER: Symbol.for('SANTANDER'),
  SCOTIABANK: Symbol.for('SCOTIABANK'),
  SECURITY: Symbol.for('SECURITY'),
});

export const ACCOUNT = Object.freeze({
  BASIC: Symbol.for('BASIC'),
  CHECKING: Symbol.for('CHECKING'),
  SAVINGS: Symbol.for('SAVINGS'),
});

export const COIN = Object.freeze({
  CLP: Symbol.for('CLP'),
});
