export default class Movement {
  constructor({ amount, motive, comment }) {
    if (typeof amount !== 'number') throw new Error('invalid movement amount');
    if (!motive || typeof motive !== 'string') throw new Error('invalid motive');
    if (comment && typeof comment !== 'string') throw new Error('invalid comment');

    this.amount = amount;
    this.motive = motive;
    if (comment) this.comment = comment;
  }
}
