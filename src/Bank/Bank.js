export default class Bank {
  constructor() {
    if (new.target === Bank) throw new Error('cannot implement abstract bank');
  }

  login = async () =>
    Promise.reject(new Error('login method not implemented on', this.constructor.name));

  accounts = async () =>
    Promise.reject(new Error('accounts method not implemented on', this.constructor.name));

  account = async () =>
    Promise.reject(new Error('account method not implemented on', this.constructor.name));

  movements = async () =>
    Promise.reject(new Error('movements method not implemented on', this.constructor.name));

  contacts = async () =>
    Promise.reject(new Error('contacts method not implemented on', this.constructor.name));

  transfer = async () =>
    Promise.reject(new Error('transfer method not implemented on', this.constructor.name));

  logout = async () =>
    Promise.reject(new Error('logout method not implemented on', this.constructor.name));
}
