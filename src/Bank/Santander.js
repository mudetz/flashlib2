import axios from 'axios';
import Bank from './Bank';
import Account from '../Account';
import Movement from '../Movement';
import Contact from '../Contact';
import Transaction from '../Transaction';
import {
  BANK,
  ACCOUNT,
} from '../types';
import {
  zeroPad,
  parseRun,
  parseNumber,
} from '../Utils';

const mapTypeToType = (type) => {
  switch (type) {
    case 'Cuenta Vista':
      return ACCOUNT.BASIC;
    case 'Cuenta de Ahorro':
      return ACCOUNT.SAVINGS;
    default:
      return ACCOUNT.CHECKING;
  }
};

const mapBankToCode = (bank) => {
  switch (bank) {
    case BANK.BANEFE:
      return '0037';
    case BANK.BBVA:
      return '0504';
    case BANK.BCI:
      return '0016';
    case BANK.BICE:
      return '0028';
    case BANK.CHILE:
      return '0001';
    case BANK.CORPBANCA:
      return '0027';
    case BANK.ESTADO:
      return '0012';
    case BANK.FALABELLA:
      return '0051';
    case BANK.INTERNACIONAL:
      return '0009';
    case BANK.ITAU:
      return '0039';
    case BANK.RIPLEY:
      return '0053';
    case BANK.SANTANDER:
      return '0035';
    case BANK.SCOTIABANK:
      return '0014';
    case BANK.SECURITY:
      return '0049';
    default:
      return '0035';
  }
};

const mapCodeToBank = (code) => {
  switch (Number(code)) {
    case 37:
      return BANK.BANEFE;
    case 504:
      return BANK.BBVA;
    case 16:
      return BANK.BCI;
    case 28:
      return BANK.BICE;
    case 1:
      return BANK.CHILE;
    case 27:
      return BANK.CORPBANCA;
    case 12:
      return BANK.ESTADO;
    case 51:
      return BANK.FALABELLA;
    case 9:
      return BANK.INTERNACIONAL;
    case 39:
      return BANK.ITAU;
    case 53:
      return BANK.RIPLEY;
    case 35:
      return BANK.SANTANDER;
    case 14:
      return BANK.SCOTIABANK;
    case 49:
      return BANK.SECURITY;
    default:
      return BANK.SANTANDER;
  }
};

const mapCodeToType = (code) => {
  switch (Number(code)) {
    case 0:
      return ACCOUNT.BASIC;
    default:
      return ACCOUNT.CHECKING;
  }
};

const mapFromTypeToCode = (type) => {
  switch (type) {
    case ACCOUNT.CHECKING:
      return '00';
    default:
      return '70';
  }
};

const mapToTypeToCode = (fromBank, type) => {
  if (fromBank === BANK.SANTANDER) switch (type) {
    case ACCOUNT.CHECKING:
      return '1';
    case ACCOUNT.SAVINGS:
      return '4';
    case ACCOUNT.BASIC:
      return '2';
    default:
      return '1';
  } else switch (type) {
    case ACCOUNT.CHECKING:
      return '20';
    case ACCOUNT.SAVINGS:
      return '10';
    case ACCOUNT.BASIC:
      return '40';
    default:
      return '20';
  }
};

class SantanderTransaction extends Transaction {
  static URL = {
    PREBEGIN: '/facade/TransferenciasATercerosInicio',
    BEGIN: '/facade/TransferenciasATercerosVerifica',
    COMMIT: '/facade/TransferenciasATercerosValSuperClave',
  };

  constructor({ conn, credentials, from, to, amount }) {
    super({ from, to, amount });
    this.conn = conn;
    this.credentials = credentials;
    this.max = {
      self: 5000000,
      others: 5000000,
    };
  }

  begin = async () => {
    let response;
    try {
      response = await this.conn.post(SantanderTransaction.URL.PREBEGIN, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, this.from.run),
          RutUsuario: zeroPad(11, this.from.run),
          InfoDispositivo: 'xx',
        },
        Mvld_Transferencias_a_Terceros_Inicio_Request: {
          INPUT: {
            'RUT-CLIENTE': zeroPad(11, this.from.run),
          },
        },
      }, {
        headers: {
          'access-token': this.credentials,
        },
      }).then(res => res.data.DATA.Mvld_Transferencias_a_Terceros_Inicio_Response.OUTPUT.ESCALARES);
      this.max = {
        self: Math.trunc(response['MONTO-MAXIMO-MISMO-BCO'] / 100),
        others: Math.trunc(response['MONTO-MAXIMO-OTROS-BCOS'] / 100),
      };

      response = await this.conn.post(SantanderTransaction.URL.BEGIN, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, this.from.run),
          RutUsuario: zeroPad(11, this.from.run),
          InfoDispositivo: 'xx',
        },
        Mvld_Transferencias_a_Terceros_Verifica_Request: {
          INPUT: {
            'RUT-CLIENTE': zeroPad(11, this.from.run),
            'CUENTA-CLIENTE': zeroPad(12, this.from.account),
            'PRODUCTO-CUENTA-CLIENTE': mapFromTypeToCode(this.from.type),
            'RUT-DESTINATARIO': zeroPad(11, this.to.run),
            'CUENTA-DESTINATARIO': zeroPad(12, this.to.account),
            'BANCO-DESTINATARIO': zeroPad(4, mapBankToCode(this.to.account.bank)),
            'TIPO-CUENTA-DESTINATARIO': mapToTypeToCode(this.from.bank, this.to.type),
            'MONTO-TRANSFERIR': this.amount.toString(),
          },
        },
      }, {
        headers: {
          'access-token': this.credentials,
        },
      }).then((res) => {
        const scalars = res.data
          .DATA
          .Mvld_Transferencias_a_Terceros_Verifica_Response
          .OUTPUT
          .ESCALARES;
        scalars.matrix = scalars.MATRIZDESAFIO.split(';');
        return scalars;
      });
    } catch (err) {
      return Promise.reject(err);
    }

    this.verificationObj = {
      'RUT-CLIENTE': zeroPad(11, this.from.run),
      'CUENTA-CLIENTE': zeroPad(12, this.from.account),
      'PRODUCTO-CUENTA-CLIENTE': mapFromTypeToCode(this.from.type),
      'EMAIL-CLIENTE': this.from.email,
      'RUT-DESTINATARIO': zeroPad(11, this.to.run),
      'CUENTA-DESTINATARIO': zeroPad(12, this.to.account),
      'PRODUCTO-CUENTA-DESTINATARIO': response['PRODUCTO-CUENTA-DESTINATARIO'],
      'BANCO-DESTINATARIO': zeroPad(4, mapBankToCode(this.to.bank)),
      'NOMBRE-DESTINATARIO': response['NOMBRE-DESTINATARIO-MB'],
      'EMAIL-DESTINATARIO': this.to.email,
      'COMENTARIO-EMAIL': 'Enviado usando la magia del cine',
      'MATRIZ-DESAFIO': '',
      'MONTO-MAX-TRANSF': zeroPad(18, 100 * this.max.others),
      'MONTO-TRANSFERIR': (100 * this.amount).toString(),
    };
    return Promise.resolve(response.matrix);
  }
  verify = async () => null;
  commit = async (...coords) => {
    let response;
    this.verificationObj['MATRIZ-DESAFIO'] = coords.join(';');
    try {
      response = await this.conn.post(SantanderTransaction.URL.COMMIT, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, this.from.run),
          RutUsuario: zeroPad(11, this.from.run),
          InfoDispositivo: 'xx',
        },
        Mvld_Transferencias_a_Terceros_Val_Super_Clave_Request: {
          INPUT: {
            ...this.verificationObj,
          },
        },
      }, {
        headers: {
          'access-token': this.credentials,
        },
      }).then(res => (
        res.data.DATA.Mvld_Transferencias_a_Terceros_Val_Super_Clave_Response.OUTPUT.INFO.CODERR
      )).then(res => Number(res));
    } catch (err) {
      return Promise.reject(err);
    }
    if (response !== 0) return Promise.reject(new Error('incorrect coordinates'));
    return Promise.resolve();
  };
}

export default class Santander extends Bank {
  static URL = {
    LOGIN: '/login',
    LOGOUT: '/logOff',
    ACCOUNTS: '/facade/CruceProductoOnline',
    MOVEMENTS: '/facade/Consultas/MvtosYDeposiDocCtas',
    CONTACTS: '/facade/UltimosDestinatarios',
  };

  constructor(params) {
    super(params);
    this.conn = axios.create({
      baseURL: 'https://apiper.santander.cl/appper/',
      timeout: 10000,
      headers: {
        connection: 'keep-alive',
      },
    });
  }

  login = async ({ run, password }) => {
    let response;
    try {
      response = await this.conn.post(Santander.URL.LOGIN, {
        RUTCLIENTE: zeroPad(11, run),
        PASSWORD: password,
        APP: '007',
        CANAL: '003',
        FILLER: '',
      }).then(res => ({ headers: res.headers, data: res.data.DATA, metadata: res.data.METADATA }));
    } catch (err) {
      return Promise.reject(err);
    }
    if (response.metadata.ERROR)
      return Promise.reject(new Error(response.data.ERROR.DESCRIPCION || 'no error description'));
    if (!response.headers['access-token'])
      return Promise.reject(new Error('contraseña incorrecta'));

    return Promise.resolve(response.headers['access-token']);
  };

  logout = async ({ credentials, run }) => {
    try {
      await this.conn.post(Santander.URL.LOGOUT, {
        CABECERA: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, run),
          RutUsuario: zeroPad(11, run),
          IpCliente: '',
          InfoDispositivo: 'valor InfoDispositivo',
        },
        RUTCLIENTE: zeroPad(11, run),
      }, {
        headers: {
          'access-token': credentials,
        },
      });
    } catch (err) {
      return Promise.reject(err);
    }
    return Promise.resolve();
  };

  accounts = async ({ credentials, run }) => {
    let response;
    try {
      response = await this.conn.post(Santander.URL.ACCOUNTS, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, run),
          RutUsuario: zeroPad(11, run),
          IpCliente: '',
          InfoDispositivo: 'xx',
        },
        INPUT: {
          'ID-RECALL': '',
          'USUARIO-ALT': '',
          ENTIDAD: '',
          TIPODOCUMENTO: '',
          NUMERODOCUMENTO: zeroPad(11, run),
          CANALACONSULTAR: '',
          CRUCEACONSULTAR: '',
          ESTADORELACION: '',
        },
      }, {
        headers: {
          'access-token': credentials,
        },
      }).then(res => ({ headers: res.headers, data: res.data.DATA, metadata: res.data.METADATA }));
    } catch (err) {
      return Promise.reject(err);
    }

    if (response.metadata.ERROR)
      return Promise.reject(new Error(response.data.ERROR.DESCRIPCION || 'no error description'));
    if (!response.data || !response.data.OUTPUT || !response.data.OUTPUT.MATRICES)
      return Promise.reject(new Error('sesión finalizada'));

    return Promise.resolve(response.data.OUTPUT.MATRICES.MATRIZCAPTACIONES.e1.filter(account => (
      ['00', '70'].includes(account.PRODUCTO)
    )).map(account => new Account({
      number: account.NUMEROCONTRATO,
      type: mapCodeToType(account.PRODUCTO),
      balance: Math.trunc(Number(account.MONTODISPONIBLE) / 100),
      bank: BANK.SANTANDER,
      coin: Symbol.for(account.CODIGOMONEDA),
    })));
  };

  movements = async ({ credentials, run }) => {
    const acts = await this.accounts({ credentials, run }).then(accounts => (
      accounts.map(account => account.number)
    ));

    let movements;
    try {
      movements = acts.map(number => this.conn.post(Santander.URL.MOVEMENTS, {
        Cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, run),
          RutUsuario: zeroPad(11, run),
          IpCliente: '',
          InfoDispositivo: 'xx',
        },
        Entrada: {
          NumeroCuenta: zeroPad(12, number),
        },
      }, {
        headers: {
          'access-token': credentials,
        },
      }).then(res => res.data.DATA.MovimientosDepositos));
      movements = await Promise.all(movements);
      movements = movements.map(account => account.slice(1).map(movement => new Movement({
        amount: Math.trunc(movement.Importe / 100),
        motive: (movement.Observa || '').trim() || (movement.CodigoAmp || '').trim(),
      })));
    } catch (err) {
      return Promise.reject(err);
    }

    const response = {};
    acts.forEach((act, i) => {
      response[act] = movements[i];
    });

    return Promise.resolve(response);
  };

  contacts = async ({ credentials, run }) => {
    let response;
    try {
      response = await this.conn.post(Santander.URL.CONTACTS, {
        cabecera: {
          HOST: {
            'USUARIO-ALT': 'GAPPS2P',
            'TERMINAL-ALT': '',
            'CANAL-ID': '078',
          },
          CanalFisico: '78',
          CanalLogico: '74',
          RutCliente: zeroPad(11, run),
          RutUsuario: zeroPad(11, run),
          IpCliente: '',
          InfoDispositivo: 'xx',
        },
        INPUT: {
          'RUT-CLIENTE': zeroPad(11, run),
        },
      }, {
        headers: {
          'access-token': credentials,
        },
      }).then(res => (
        res.data.DATA
          .Mvld_SP_Consulta_Ult_Destinatarios_Response
          .OUTPUT
          .MATRICES['MATRIZ-ULTIMOS-DESTINATARIOS']
          .e
      ));
    } catch (err) {
      return Promise.reject(err);
    }

    return Promise.resolve(response.map(contact => new Contact({
      run: parseRun(contact['RUT-CLIENTE']),
      account: parseNumber(contact['NUMERO-CUENTA']),
      bank: mapCodeToBank(contact['CODIGO-BANCO-CCA']),
      type: mapTypeToType(contact.DESC_TIPO_CUENTA),
      name: contact['NOMBRE-CLIENTE'],
      email: contact.CORREO_ELECTRONICO,
    })));
  };

  transfer = async ({ credentials, from, to, amount }) => new SantanderTransaction({
    ...this,
    from,
    to,
    amount,
    credentials,
  });
}
