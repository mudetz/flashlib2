import {
  BANK,
  ACCOUNT,
} from './types';

import {
  Santander,
} from './Bank';

import {
  isValidRun,
  parseRun,
} from './Utils';

export default class Flashlib {
  static BANK = BANK;
  static ACCOUNT = ACCOUNT;

  constructor({ run, password, bank }) {
    if (!run || !isValidRun(parseRun(run))) throw new Error('run invalido');
    if (!password || typeof password !== 'string') throw new Error('contraseña inválida');
    if (!bank || !Object.values(Flashlib.BANK).includes(bank)) throw new Error('banco inválido');

    this.run = parseRun(run);
    this.password = password;

    switch (bank) {
      case Flashlib.BANK.SANTANDER:
        this.account = new Santander();
        break;
      default:
        throw new Error('banco inválido');
    }
  }

  login = async () => {
    try {
      this.credentials = await this.account.login({ ...this });
      return Promise.resolve();
    } catch (err) {
      return Promise.reject(err);
    }
  }

  accounts = async () => this.account.accounts({ ...this });

  account = async ({ number }) => this.account.account({ ...this, number });

  movements = async () => this.account.movements({ ...this });

  contacts = async () => this.account.contacts({ ...this });

  transfer = async ({ from, to, amount }) => this.account.transfer({ ...this, from, to, amount });

  logout = async () => this.account.logout({ ...this });
}
