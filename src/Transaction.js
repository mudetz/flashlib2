import Contact from './Contact';

export default class Transaction {
  constructor({ from, to, amount }) {
    if (new.target === Transaction) throw new Error('cannot implement abstract transaction');
    if (!(from instanceof Contact)) throw new Error(`invalid 'from' contact ${from}`);
    if (!(to instanceof Contact)) throw new Error(`invalid 'to' contact ${to}`);
    if (typeof amount !== 'number') throw new Error(`invalid amout ${amount}`);
    if (amount < 1000) throw new Error(`amount must not be smaller than 1000 (${amount} < 1000)`);

    this.from = from;
    this.to = to;
    this.amount = amount;
  }

  begin = async () =>
    Promise.reject(new Error('begin method not implemented on', this.constructor.name));

  validate = async () =>
    Promise.reject(new Error('validate method not implemented on', this.constructor.name));

  commit = async () =>
    Promise.reject(new Error('commit method not implemented on', this.constructor.name));
}
