import { ACCOUNT, COIN, BANK } from './types';
import { parseNumber } from './Utils';

export default class Account {
  constructor({ number, type, balance, bank, coin, hash }) {
    if (!number || !parseNumber(number)) throw new Error('numero de cuenta invalido');
    if (!type || !Object.values(ACCOUNT).includes(type)) throw new Error('tipo de cuenta invalido');
    if (typeof balance !== 'number') throw new Error('balance invalido');
    if (!bank || !Object.values(BANK).includes(bank)) throw new Error('banco inválido');
    if (!coin || !Object.values(COIN).includes(coin)) throw new Error('moneda no soportada');

    this.number = parseNumber(number);
    this.type = type;
    this.balance = balance;
    this.bank = bank;
    this.coin = coin;
    if (hash) this.hash = hash;
  }
}
