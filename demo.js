import dotenv from 'dotenv';
import prompt from 'prompt';
import Flashlib from './lib/flashlib';
import Contact from './lib/Contact';

dotenv.config();

(async () => {
  const from = new Contact({
    run: process.env.FROM_RUN,
    account: Number(process.env.FROM_ACCOUNT),
    bank: Flashlib.BANK.SANTANDER,
    type: Flashlib.ACCOUNT.CHECKING,
    name: '',
    email: '',
  });

  const to = new Contact({
    run: process.env.TO_RUN,
    account: Number(process.env.TO_ACCOUNT),
    bank: Flashlib.BANK.SANTANDER,
    type: Flashlib.ACCOUNT.BASIC,
    name: '',
    email: '',
  });

  const account = new Flashlib({
    run: process.env.RUN,
    password: process.env.PASSWORD,
    bank: Flashlib.BANK.SANTANDER,
  });

  const login = await account.login();
  console.log('login()', login, '\n');

  const accounts = await account.accounts();
  console.log('accounts()', accounts, '\n');

  const movements = await account.movements();
  console.log('movements()', movements, '\n');

  const contacts = await account.contacts();
  console.log('contacts()', contacts, '\n');

  const transaction = await account.transfer({ from, to, amount: 2000 });
  const coords = await transaction.begin();
  console.log('transaction.begin()', coords);

  await prompt.start();
  await prompt.get([coords.join(';')], async (err, result) => {
    const input = result[coords.join(';')].split(';');
    await transaction.verify(null);
    const commitment = await transaction.commit(...input);
    console.log('transaction.commit()', commitment);

    const logout = await account.logout();
    console.log(logout);
  });
})();
