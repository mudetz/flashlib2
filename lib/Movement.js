'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Movement = function Movement(_ref) {
  var amount = _ref.amount,
      motive = _ref.motive,
      comment = _ref.comment;

  _classCallCheck(this, Movement);

  if (typeof amount !== 'number') throw new Error('invalid movement amount');
  if (!motive || typeof motive !== 'string') throw new Error('invalid motive');
  if (comment && typeof comment !== 'string') throw new Error('invalid comment');

  this.amount = amount;
  this.motive = motive;
  if (comment) this.comment = comment;
};

exports.default = Movement;