'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Utils = require('./Utils');

var _types = require('./types');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

var Contact = function Contact(_ref) {
  var run = _ref.run,
      account = _ref.account,
      bank = _ref.bank,
      type = _ref.type,
      name = _ref.name,
      email = _ref.email;

  _classCallCheck(this, Contact);

  if (!(0, _Utils.isValidRun)((0, _Utils.parseRun)(run))) throw new Error('invalid run ' + run);
  if (!account || typeof account !== 'number') throw new Error('invalid account');
  if (!bank || !Object.values(_types.BANK).includes(bank)) throw new Error('unsupported bank');
  if (!type || !Object.values(_types.ACCOUNT).includes(type)) throw new Error('invalid account type');
  if (!name || typeof name !== 'string') throw new Error('invalid name');
  if (email && !emailRegex.test(email)) throw new Error('invalid email');

  this.run = (0, _Utils.parseRun)(run);
  this.account = (0, _Utils.parseNumber)(account);
  this.bank = bank;
  this.type = type;
  this.name = name;
  if (email) this.email = email;
};

exports.default = Contact;