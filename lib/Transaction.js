'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Contact = require('./Contact');

var _Contact2 = _interopRequireDefault(_Contact);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Transaction = function Transaction(_ref) {
  var _this = this;

  var from = _ref.from,
      to = _ref.to,
      amount = _ref.amount;

  _classCallCheck(this, Transaction);

  this.begin = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            return _context.abrupt('return', Promise.reject(new Error('begin method not implemented on', _this.constructor.name)));

          case 1:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, _this);
  }));
  this.validate = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt('return', Promise.reject(new Error('validate method not implemented on', _this.constructor.name)));

          case 1:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));
  this.commit = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            return _context3.abrupt('return', Promise.reject(new Error('commit method not implemented on', _this.constructor.name)));

          case 1:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, _this);
  }));

  if (new.target === Transaction) throw new Error('cannot implement abstract transaction');
  if (!(from instanceof _Contact2.default)) throw new Error('invalid \'from\' contact ' + from);
  if (!(to instanceof _Contact2.default)) throw new Error('invalid \'to\' contact ' + to);
  if (typeof amount !== 'number') throw new Error('invalid amout ' + amount);
  if (amount < 1000) throw new Error('amount must not be smaller than 1000 (' + amount + ' < 1000)');

  this.from = from;
  this.to = to;
  this.amount = amount;
};

exports.default = Transaction;