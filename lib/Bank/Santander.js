'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _Bank2 = require('./Bank');

var _Bank3 = _interopRequireDefault(_Bank2);

var _Account = require('../Account');

var _Account2 = _interopRequireDefault(_Account);

var _Movement = require('../Movement');

var _Movement2 = _interopRequireDefault(_Movement);

var _Contact = require('../Contact');

var _Contact2 = _interopRequireDefault(_Contact);

var _Transaction2 = require('../Transaction');

var _Transaction3 = _interopRequireDefault(_Transaction2);

var _types = require('../types');

var _Utils = require('../Utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var mapTypeToType = function mapTypeToType(type) {
  switch (type) {
    case 'Cuenta Vista':
      return _types.ACCOUNT.BASIC;
    case 'Cuenta de Ahorro':
      return _types.ACCOUNT.SAVINGS;
    default:
      return _types.ACCOUNT.CHECKING;
  }
};

var mapBankToCode = function mapBankToCode(bank) {
  switch (bank) {
    case _types.BANK.BANEFE:
      return '0037';
    case _types.BANK.BBVA:
      return '0504';
    case _types.BANK.BCI:
      return '0016';
    case _types.BANK.BICE:
      return '0028';
    case _types.BANK.CHILE:
      return '0001';
    case _types.BANK.CORPBANCA:
      return '0027';
    case _types.BANK.ESTADO:
      return '0012';
    case _types.BANK.FALABELLA:
      return '0051';
    case _types.BANK.INTERNACIONAL:
      return '0009';
    case _types.BANK.ITAU:
      return '0039';
    case _types.BANK.RIPLEY:
      return '0053';
    case _types.BANK.SANTANDER:
      return '0035';
    case _types.BANK.SCOTIABANK:
      return '0014';
    case _types.BANK.SECURITY:
      return '0049';
    default:
      return '0035';
  }
};

var mapCodeToBank = function mapCodeToBank(code) {
  switch (Number(code)) {
    case 37:
      return _types.BANK.BANEFE;
    case 504:
      return _types.BANK.BBVA;
    case 16:
      return _types.BANK.BCI;
    case 28:
      return _types.BANK.BICE;
    case 1:
      return _types.BANK.CHILE;
    case 27:
      return _types.BANK.CORPBANCA;
    case 12:
      return _types.BANK.ESTADO;
    case 51:
      return _types.BANK.FALABELLA;
    case 9:
      return _types.BANK.INTERNACIONAL;
    case 39:
      return _types.BANK.ITAU;
    case 53:
      return _types.BANK.RIPLEY;
    case 35:
      return _types.BANK.SANTANDER;
    case 14:
      return _types.BANK.SCOTIABANK;
    case 49:
      return _types.BANK.SECURITY;
    default:
      return _types.BANK.SANTANDER;
  }
};

var mapCodeToType = function mapCodeToType(code) {
  switch (Number(code)) {
    case 0:
      return _types.ACCOUNT.BASIC;
    default:
      return _types.ACCOUNT.CHECKING;
  }
};

var mapFromTypeToCode = function mapFromTypeToCode(type) {
  switch (type) {
    case _types.ACCOUNT.CHECKING:
      return '00';
    default:
      return '70';
  }
};

var mapToTypeToCode = function mapToTypeToCode(fromBank, type) {
  if (fromBank === _types.BANK.SANTANDER) switch (type) {
    case _types.ACCOUNT.CHECKING:
      return '1';
    case _types.ACCOUNT.SAVINGS:
      return '4';
    case _types.ACCOUNT.BASIC:
      return '2';
    default:
      return '1';
  } else switch (type) {
    case _types.ACCOUNT.CHECKING:
      return '20';
    case _types.ACCOUNT.SAVINGS:
      return '10';
    case _types.ACCOUNT.BASIC:
      return '40';
    default:
      return '20';
  }
};

var SantanderTransaction = function (_Transaction) {
  _inherits(SantanderTransaction, _Transaction);

  function SantanderTransaction(_ref) {
    var _this2 = this;

    var conn = _ref.conn,
        credentials = _ref.credentials,
        from = _ref.from,
        to = _ref.to,
        amount = _ref.amount;

    _classCallCheck(this, SantanderTransaction);

    var _this = _possibleConstructorReturn(this, (SantanderTransaction.__proto__ || Object.getPrototypeOf(SantanderTransaction)).call(this, { from: from, to: to, amount: amount }));

    _this.begin = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
      var response;
      return regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              response = void 0;
              _context.prev = 1;
              _context.next = 4;
              return _this.conn.post(SantanderTransaction.URL.PREBEGIN, {
                cabecera: {
                  HOST: {
                    'USUARIO-ALT': 'GAPPS2P',
                    'TERMINAL-ALT': '',
                    'CANAL-ID': '078'
                  },
                  CanalFisico: '78',
                  CanalLogico: '74',
                  RutCliente: (0, _Utils.zeroPad)(11, _this.from.run),
                  RutUsuario: (0, _Utils.zeroPad)(11, _this.from.run),
                  InfoDispositivo: 'xx'
                },
                Mvld_Transferencias_a_Terceros_Inicio_Request: {
                  INPUT: {
                    'RUT-CLIENTE': (0, _Utils.zeroPad)(11, _this.from.run)
                  }
                }
              }, {
                headers: {
                  'access-token': _this.credentials
                }
              }).then(function (res) {
                return res.data.DATA.Mvld_Transferencias_a_Terceros_Inicio_Response.OUTPUT.ESCALARES;
              });

            case 4:
              response = _context.sent;

              _this.max = {
                self: Math.trunc(response['MONTO-MAXIMO-MISMO-BCO'] / 100),
                others: Math.trunc(response['MONTO-MAXIMO-OTROS-BCOS'] / 100)
              };

              _context.next = 8;
              return _this.conn.post(SantanderTransaction.URL.BEGIN, {
                cabecera: {
                  HOST: {
                    'USUARIO-ALT': 'GAPPS2P',
                    'TERMINAL-ALT': '',
                    'CANAL-ID': '078'
                  },
                  CanalFisico: '78',
                  CanalLogico: '74',
                  RutCliente: (0, _Utils.zeroPad)(11, _this.from.run),
                  RutUsuario: (0, _Utils.zeroPad)(11, _this.from.run),
                  InfoDispositivo: 'xx'
                },
                Mvld_Transferencias_a_Terceros_Verifica_Request: {
                  INPUT: {
                    'RUT-CLIENTE': (0, _Utils.zeroPad)(11, _this.from.run),
                    'CUENTA-CLIENTE': (0, _Utils.zeroPad)(12, _this.from.account),
                    'PRODUCTO-CUENTA-CLIENTE': mapFromTypeToCode(_this.from.type),
                    'RUT-DESTINATARIO': (0, _Utils.zeroPad)(11, _this.to.run),
                    'CUENTA-DESTINATARIO': (0, _Utils.zeroPad)(12, _this.to.account),
                    'BANCO-DESTINATARIO': (0, _Utils.zeroPad)(4, mapBankToCode(_this.to.account.bank)),
                    'TIPO-CUENTA-DESTINATARIO': mapToTypeToCode(_this.from.bank, _this.to.type),
                    'MONTO-TRANSFERIR': _this.amount.toString()
                  }
                }
              }, {
                headers: {
                  'access-token': _this.credentials
                }
              }).then(function (res) {
                var scalars = res.data.DATA.Mvld_Transferencias_a_Terceros_Verifica_Response.OUTPUT.ESCALARES;
                scalars.matrix = scalars.MATRIZDESAFIO.split(';');
                return scalars;
              });

            case 8:
              response = _context.sent;
              _context.next = 14;
              break;

            case 11:
              _context.prev = 11;
              _context.t0 = _context['catch'](1);
              return _context.abrupt('return', Promise.reject(_context.t0));

            case 14:

              _this.verificationObj = {
                'RUT-CLIENTE': (0, _Utils.zeroPad)(11, _this.from.run),
                'CUENTA-CLIENTE': (0, _Utils.zeroPad)(12, _this.from.account),
                'PRODUCTO-CUENTA-CLIENTE': mapFromTypeToCode(_this.from.type),
                'EMAIL-CLIENTE': _this.from.email,
                'RUT-DESTINATARIO': (0, _Utils.zeroPad)(11, _this.to.run),
                'CUENTA-DESTINATARIO': (0, _Utils.zeroPad)(12, _this.to.account),
                'PRODUCTO-CUENTA-DESTINATARIO': response['PRODUCTO-CUENTA-DESTINATARIO'],
                'BANCO-DESTINATARIO': (0, _Utils.zeroPad)(4, mapBankToCode(_this.to.bank)),
                'NOMBRE-DESTINATARIO': response['NOMBRE-DESTINATARIO-MB'],
                'EMAIL-DESTINATARIO': _this.to.email,
                'COMENTARIO-EMAIL': 'Enviado usando la magia del cine',
                'MATRIZ-DESAFIO': '',
                'MONTO-MAX-TRANSF': (0, _Utils.zeroPad)(18, 100 * _this.max.others),
                'MONTO-TRANSFERIR': (100 * _this.amount).toString()
              };
              return _context.abrupt('return', Promise.resolve(response.matrix));

            case 16:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this2, [[1, 11]]);
    }));
    _this.verify = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
      return regeneratorRuntime.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.abrupt('return', null);

            case 1:
            case 'end':
              return _context2.stop();
          }
        }
      }, _callee2, _this2);
    }));

    _this.commit = function () {
      var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
        for (var _len = arguments.length, coords = Array(_len), _key = 0; _key < _len; _key++) {
          coords[_key] = arguments[_key];
        }

        var response;
        return regeneratorRuntime.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                response = void 0;

                _this.verificationObj['MATRIZ-DESAFIO'] = coords.join(';');
                _context3.prev = 2;
                _context3.next = 5;
                return _this.conn.post(SantanderTransaction.URL.COMMIT, {
                  cabecera: {
                    HOST: {
                      'USUARIO-ALT': 'GAPPS2P',
                      'TERMINAL-ALT': '',
                      'CANAL-ID': '078'
                    },
                    CanalFisico: '78',
                    CanalLogico: '74',
                    RutCliente: (0, _Utils.zeroPad)(11, _this.from.run),
                    RutUsuario: (0, _Utils.zeroPad)(11, _this.from.run),
                    InfoDispositivo: 'xx'
                  },
                  Mvld_Transferencias_a_Terceros_Val_Super_Clave_Request: {
                    INPUT: _extends({}, _this.verificationObj)
                  }
                }, {
                  headers: {
                    'access-token': _this.credentials
                  }
                }).then(function (res) {
                  return res.data.DATA.Mvld_Transferencias_a_Terceros_Val_Super_Clave_Response.OUTPUT.INFO.CODERR;
                }).then(function (res) {
                  return Number(res);
                });

              case 5:
                response = _context3.sent;
                _context3.next = 11;
                break;

              case 8:
                _context3.prev = 8;
                _context3.t0 = _context3['catch'](2);
                return _context3.abrupt('return', Promise.reject(_context3.t0));

              case 11:
                if (!(response !== 0)) {
                  _context3.next = 13;
                  break;
                }

                return _context3.abrupt('return', Promise.reject(new Error('incorrect coordinates')));

              case 13:
                return _context3.abrupt('return', Promise.resolve());

              case 14:
              case 'end':
                return _context3.stop();
            }
          }
        }, _callee3, _this2, [[2, 8]]);
      }));

      return function () {
        return _ref4.apply(this, arguments);
      };
    }();

    _this.conn = conn;
    _this.credentials = credentials;
    _this.max = {
      self: 5000000,
      others: 5000000
    };
    return _this;
  }

  return SantanderTransaction;
}(_Transaction3.default);

SantanderTransaction.URL = {
  PREBEGIN: '/facade/TransferenciasATercerosInicio',
  BEGIN: '/facade/TransferenciasATercerosVerifica',
  COMMIT: '/facade/TransferenciasATercerosValSuperClave'
};

var Santander = function (_Bank) {
  _inherits(Santander, _Bank);

  function Santander(params) {
    var _this4 = this;

    _classCallCheck(this, Santander);

    var _this3 = _possibleConstructorReturn(this, (Santander.__proto__ || Object.getPrototypeOf(Santander)).call(this, params));

    _this3.login = function () {
      var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(_ref6) {
        var run = _ref6.run,
            password = _ref6.password;
        var response;
        return regeneratorRuntime.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                response = void 0;
                _context4.prev = 1;
                _context4.next = 4;
                return _this3.conn.post(Santander.URL.LOGIN, {
                  RUTCLIENTE: (0, _Utils.zeroPad)(11, run),
                  PASSWORD: password,
                  APP: '007',
                  CANAL: '003',
                  FILLER: ''
                }).then(function (res) {
                  return { headers: res.headers, data: res.data.DATA, metadata: res.data.METADATA };
                });

              case 4:
                response = _context4.sent;
                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4['catch'](1);
                return _context4.abrupt('return', Promise.reject(_context4.t0));

              case 10:
                if (!response.metadata.ERROR) {
                  _context4.next = 12;
                  break;
                }

                return _context4.abrupt('return', Promise.reject(new Error(response.data.ERROR.DESCRIPCION || 'no error description')));

              case 12:
                if (response.headers['access-token']) {
                  _context4.next = 14;
                  break;
                }

                return _context4.abrupt('return', Promise.reject(new Error('contraseña incorrecta')));

              case 14:
                return _context4.abrupt('return', Promise.resolve(response.headers['access-token']));

              case 15:
              case 'end':
                return _context4.stop();
            }
          }
        }, _callee4, _this4, [[1, 7]]);
      }));

      return function (_x) {
        return _ref5.apply(this, arguments);
      };
    }();

    _this3.logout = function () {
      var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(_ref8) {
        var credentials = _ref8.credentials,
            run = _ref8.run;
        return regeneratorRuntime.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _this3.conn.post(Santander.URL.LOGOUT, {
                  CABECERA: {
                    HOST: {
                      'USUARIO-ALT': 'GAPPS2P',
                      'TERMINAL-ALT': '',
                      'CANAL-ID': '078'
                    },
                    CanalFisico: '78',
                    CanalLogico: '74',
                    RutCliente: (0, _Utils.zeroPad)(11, run),
                    RutUsuario: (0, _Utils.zeroPad)(11, run),
                    IpCliente: '',
                    InfoDispositivo: 'valor InfoDispositivo'
                  },
                  RUTCLIENTE: (0, _Utils.zeroPad)(11, run)
                }, {
                  headers: {
                    'access-token': credentials
                  }
                });

              case 3:
                _context5.next = 8;
                break;

              case 5:
                _context5.prev = 5;
                _context5.t0 = _context5['catch'](0);
                return _context5.abrupt('return', Promise.reject(_context5.t0));

              case 8:
                return _context5.abrupt('return', Promise.resolve());

              case 9:
              case 'end':
                return _context5.stop();
            }
          }
        }, _callee5, _this4, [[0, 5]]);
      }));

      return function (_x2) {
        return _ref7.apply(this, arguments);
      };
    }();

    _this3.accounts = function () {
      var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(_ref10) {
        var credentials = _ref10.credentials,
            run = _ref10.run;
        var response;
        return regeneratorRuntime.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                response = void 0;
                _context6.prev = 1;
                _context6.next = 4;
                return _this3.conn.post(Santander.URL.ACCOUNTS, {
                  cabecera: {
                    HOST: {
                      'USUARIO-ALT': 'GAPPS2P',
                      'TERMINAL-ALT': '',
                      'CANAL-ID': '078'
                    },
                    CanalFisico: '78',
                    CanalLogico: '74',
                    RutCliente: (0, _Utils.zeroPad)(11, run),
                    RutUsuario: (0, _Utils.zeroPad)(11, run),
                    IpCliente: '',
                    InfoDispositivo: 'xx'
                  },
                  INPUT: {
                    'ID-RECALL': '',
                    'USUARIO-ALT': '',
                    ENTIDAD: '',
                    TIPODOCUMENTO: '',
                    NUMERODOCUMENTO: (0, _Utils.zeroPad)(11, run),
                    CANALACONSULTAR: '',
                    CRUCEACONSULTAR: '',
                    ESTADORELACION: ''
                  }
                }, {
                  headers: {
                    'access-token': credentials
                  }
                }).then(function (res) {
                  return { headers: res.headers, data: res.data.DATA, metadata: res.data.METADATA };
                });

              case 4:
                response = _context6.sent;
                _context6.next = 10;
                break;

              case 7:
                _context6.prev = 7;
                _context6.t0 = _context6['catch'](1);
                return _context6.abrupt('return', Promise.reject(_context6.t0));

              case 10:
                if (!response.metadata.ERROR) {
                  _context6.next = 12;
                  break;
                }

                return _context6.abrupt('return', Promise.reject(new Error(response.data.ERROR.DESCRIPCION || 'no error description')));

              case 12:
                if (!(!response.data || !response.data.OUTPUT || !response.data.OUTPUT.MATRICES)) {
                  _context6.next = 14;
                  break;
                }

                return _context6.abrupt('return', Promise.reject(new Error('sesión finalizada')));

              case 14:
                return _context6.abrupt('return', Promise.resolve(response.data.OUTPUT.MATRICES.MATRIZCAPTACIONES.e1.filter(function (account) {
                  return ['00', '70'].includes(account.PRODUCTO);
                }).map(function (account) {
                  return new _Account2.default({
                    number: account.NUMEROCONTRATO,
                    type: mapCodeToType(account.PRODUCTO),
                    balance: Math.trunc(Number(account.MONTODISPONIBLE) / 100),
                    bank: _types.BANK.SANTANDER,
                    coin: Symbol.for(account.CODIGOMONEDA)
                  });
                })));

              case 15:
              case 'end':
                return _context6.stop();
            }
          }
        }, _callee6, _this4, [[1, 7]]);
      }));

      return function (_x3) {
        return _ref9.apply(this, arguments);
      };
    }();

    _this3.movements = function () {
      var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(_ref12) {
        var credentials = _ref12.credentials,
            run = _ref12.run;
        var acts, movements, response;
        return regeneratorRuntime.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.next = 2;
                return _this3.accounts({ credentials: credentials, run: run }).then(function (accounts) {
                  return accounts.map(function (account) {
                    return account.number;
                  });
                });

              case 2:
                acts = _context7.sent;
                movements = void 0;
                _context7.prev = 4;

                movements = acts.map(function (number) {
                  return _this3.conn.post(Santander.URL.MOVEMENTS, {
                    Cabecera: {
                      HOST: {
                        'USUARIO-ALT': 'GAPPS2P',
                        'TERMINAL-ALT': '',
                        'CANAL-ID': '078'
                      },
                      CanalFisico: '78',
                      CanalLogico: '74',
                      RutCliente: (0, _Utils.zeroPad)(11, run),
                      RutUsuario: (0, _Utils.zeroPad)(11, run),
                      IpCliente: '',
                      InfoDispositivo: 'xx'
                    },
                    Entrada: {
                      NumeroCuenta: (0, _Utils.zeroPad)(12, number)
                    }
                  }, {
                    headers: {
                      'access-token': credentials
                    }
                  }).then(function (res) {
                    return res.data.DATA.MovimientosDepositos;
                  });
                });
                _context7.next = 8;
                return Promise.all(movements);

              case 8:
                movements = _context7.sent;

                movements = movements.map(function (account) {
                  return account.slice(1).map(function (movement) {
                    return new _Movement2.default({
                      amount: Math.trunc(movement.Importe / 100),
                      motive: (movement.Observa || '').trim() || (movement.CodigoAmp || '').trim()
                    });
                  });
                });
                _context7.next = 15;
                break;

              case 12:
                _context7.prev = 12;
                _context7.t0 = _context7['catch'](4);
                return _context7.abrupt('return', Promise.reject(_context7.t0));

              case 15:
                response = {};

                acts.forEach(function (act, i) {
                  response[act] = movements[i];
                });

                return _context7.abrupt('return', Promise.resolve(response));

              case 18:
              case 'end':
                return _context7.stop();
            }
          }
        }, _callee7, _this4, [[4, 12]]);
      }));

      return function (_x4) {
        return _ref11.apply(this, arguments);
      };
    }();

    _this3.contacts = function () {
      var _ref13 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(_ref14) {
        var credentials = _ref14.credentials,
            run = _ref14.run;
        var response;
        return regeneratorRuntime.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                response = void 0;
                _context8.prev = 1;
                _context8.next = 4;
                return _this3.conn.post(Santander.URL.CONTACTS, {
                  cabecera: {
                    HOST: {
                      'USUARIO-ALT': 'GAPPS2P',
                      'TERMINAL-ALT': '',
                      'CANAL-ID': '078'
                    },
                    CanalFisico: '78',
                    CanalLogico: '74',
                    RutCliente: (0, _Utils.zeroPad)(11, run),
                    RutUsuario: (0, _Utils.zeroPad)(11, run),
                    IpCliente: '',
                    InfoDispositivo: 'xx'
                  },
                  INPUT: {
                    'RUT-CLIENTE': (0, _Utils.zeroPad)(11, run)
                  }
                }, {
                  headers: {
                    'access-token': credentials
                  }
                }).then(function (res) {
                  return res.data.DATA.Mvld_SP_Consulta_Ult_Destinatarios_Response.OUTPUT.MATRICES['MATRIZ-ULTIMOS-DESTINATARIOS'].e;
                });

              case 4:
                response = _context8.sent;
                _context8.next = 10;
                break;

              case 7:
                _context8.prev = 7;
                _context8.t0 = _context8['catch'](1);
                return _context8.abrupt('return', Promise.reject(_context8.t0));

              case 10:
                return _context8.abrupt('return', Promise.resolve(response.map(function (contact) {
                  return new _Contact2.default({
                    run: (0, _Utils.parseRun)(contact['RUT-CLIENTE']),
                    account: (0, _Utils.parseNumber)(contact['NUMERO-CUENTA']),
                    bank: mapCodeToBank(contact['CODIGO-BANCO-CCA']),
                    type: mapTypeToType(contact.DESC_TIPO_CUENTA),
                    name: contact['NOMBRE-CLIENTE'],
                    email: contact.CORREO_ELECTRONICO
                  });
                })));

              case 11:
              case 'end':
                return _context8.stop();
            }
          }
        }, _callee8, _this4, [[1, 7]]);
      }));

      return function (_x5) {
        return _ref13.apply(this, arguments);
      };
    }();

    _this3.transfer = function () {
      var _ref15 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(_ref16) {
        var credentials = _ref16.credentials,
            from = _ref16.from,
            to = _ref16.to,
            amount = _ref16.amount;
        return regeneratorRuntime.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                return _context9.abrupt('return', new SantanderTransaction(_extends({}, _this3, {
                  from: from,
                  to: to,
                  amount: amount,
                  credentials: credentials
                })));

              case 1:
              case 'end':
                return _context9.stop();
            }
          }
        }, _callee9, _this4);
      }));

      return function (_x6) {
        return _ref15.apply(this, arguments);
      };
    }();

    _this3.conn = _axios2.default.create({
      baseURL: 'https://apiper.santander.cl/appper/',
      timeout: 10000,
      headers: {
        connection: 'keep-alive'
      }
    });
    return _this3;
  }

  return Santander;
}(_Bank3.default);

Santander.URL = {
  LOGIN: '/login',
  LOGOUT: '/logOff',
  ACCOUNTS: '/facade/CruceProductoOnline',
  MOVEMENTS: '/facade/Consultas/MvtosYDeposiDocCtas',
  CONTACTS: '/facade/UltimosDestinatarios'
};
exports.default = Santander;