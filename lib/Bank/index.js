'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _Santander = require('./Santander');

Object.defineProperty(exports, 'Santander', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_Santander).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }