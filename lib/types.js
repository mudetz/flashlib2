'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var BANK = exports.BANK = Object.freeze({
  BANEFE: Symbol.for('BANEFE'),
  BBVA: Symbol.for('BBVA'),
  BCI: Symbol.for('BCI'),
  BCINOVA: Symbol.for('BCINOVA'),
  BICE: Symbol.for('BICE'),
  CHILE: Symbol.for('CHILE'),
  CONDELL: Symbol.for('CONDELL'),
  CORPBANCA: Symbol.for('CORPBANCA'),
  ESTADO: Symbol.for('ESTADO'),
  FALABELLA: Symbol.for('FALABELLA'),
  INTERNACIONAL: Symbol.for('INTERNACIONAL'),
  ITAU: Symbol.for('ITAU'),
  RIPLEY: Symbol.for('RIPLEY'),
  SANTANDER: Symbol.for('SANTANDER'),
  SCOTIABANK: Symbol.for('SCOTIABANK'),
  SECURITY: Symbol.for('SECURITY')
});

var ACCOUNT = exports.ACCOUNT = Object.freeze({
  BASIC: Symbol.for('BASIC'),
  CHECKING: Symbol.for('CHECKING'),
  SAVINGS: Symbol.for('SAVINGS')
});

var COIN = exports.COIN = Object.freeze({
  CLP: Symbol.for('CLP')
});