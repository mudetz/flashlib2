'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var parseRun = function parseRun(run) {
  if (typeof run === 'number') return run.toString();else if (typeof run === 'string') return run.replace(/[^0123456789K]/g, '').toUpperCase();
  return null;
};

exports.default = parseRun;