'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var dv = function dv(X) {
  var T = Number(X);
  var M = 0;
  var S = 1;
  for (; T; T = Math.floor(T / 10)) {
    S = (S + T % 10 * (9 - M % 6)) % 11;
    M += 1;
  }
  return S ? (S - 1).toString() : 'K';
};

var isValidRun = function isValidRun(run) {
  if (!run || typeof run !== 'string') return false;
  if (!/^\d{7,}(?:\d|K)$/.test(run)) return false;
  return run.slice(-1) === dv(run.slice(0, -1));
};

exports.default = isValidRun;