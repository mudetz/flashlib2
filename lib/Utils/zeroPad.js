'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var zeroPad = function zeroPad(n, str) {
  return '0'.repeat(n).concat(str).slice(-n);
};
exports.default = zeroPad;