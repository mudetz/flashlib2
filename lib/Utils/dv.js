'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var dv = function dv(X) {
  var T = X;
  var M = 0;
  var S = 1;
  for (; T; T = Math.floor(T / 10)) {
    S = (S + T % 10 * (9 - M % 6)) % 11;
    M += 1;
  }
  return S ? (S - 1).toString() : 'K';
};

exports.default = dv;