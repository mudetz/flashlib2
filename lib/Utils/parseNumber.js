'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
var parseNumber = function parseNumber(number) {
  if (typeof number === 'number') return number;else if (typeof number === 'string') return Number(number.replace(/[^0123456789]/gi, ''));
  return null;
};

exports.default = parseNumber;