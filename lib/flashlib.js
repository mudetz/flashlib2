'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _types = require('./types');

var _Bank = require('./Bank');

var _Utils = require('./Utils');

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Flashlib = function Flashlib(_ref) {
  var _this = this;

  var run = _ref.run,
      password = _ref.password,
      bank = _ref.bank;

  _classCallCheck(this, Flashlib);

  this.login = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.prev = 0;
            _context.next = 3;
            return _this.account.login(_extends({}, _this));

          case 3:
            _this.credentials = _context.sent;
            return _context.abrupt('return', Promise.resolve());

          case 7:
            _context.prev = 7;
            _context.t0 = _context['catch'](0);
            return _context.abrupt('return', Promise.reject(_context.t0));

          case 10:
          case 'end':
            return _context.stop();
        }
      }
    }, _callee, _this, [[0, 7]]);
  }));
  this.accounts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            return _context2.abrupt('return', _this.account.accounts(_extends({}, _this)));

          case 1:
          case 'end':
            return _context2.stop();
        }
      }
    }, _callee2, _this);
  }));

  this.account = function () {
    var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(_ref5) {
      var number = _ref5.number;
      return regeneratorRuntime.wrap(function _callee3$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              return _context3.abrupt('return', _this.account.account(_extends({}, _this, { number: number })));

            case 1:
            case 'end':
              return _context3.stop();
          }
        }
      }, _callee3, _this);
    }));

    return function (_x) {
      return _ref4.apply(this, arguments);
    };
  }();

  this.movements = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            return _context4.abrupt('return', _this.account.movements(_extends({}, _this)));

          case 1:
          case 'end':
            return _context4.stop();
        }
      }
    }, _callee4, _this);
  }));
  this.contacts = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            return _context5.abrupt('return', _this.account.contacts(_extends({}, _this)));

          case 1:
          case 'end':
            return _context5.stop();
        }
      }
    }, _callee5, _this);
  }));

  this.transfer = function () {
    var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(_ref9) {
      var from = _ref9.from,
          to = _ref9.to,
          amount = _ref9.amount;
      return regeneratorRuntime.wrap(function _callee6$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              return _context6.abrupt('return', _this.account.transfer(_extends({}, _this, { from: from, to: to, amount: amount })));

            case 1:
            case 'end':
              return _context6.stop();
          }
        }
      }, _callee6, _this);
    }));

    return function (_x2) {
      return _ref8.apply(this, arguments);
    };
  }();

  this.logout = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            return _context7.abrupt('return', _this.account.logout(_extends({}, _this)));

          case 1:
          case 'end':
            return _context7.stop();
        }
      }
    }, _callee7, _this);
  }));

  if (!run || !(0, _Utils.isValidRun)((0, _Utils.parseRun)(run))) throw new Error('run invalido');
  if (!password || typeof password !== 'string') throw new Error('contraseña inválida');
  if (!bank || !Object.values(Flashlib.BANK).includes(bank)) throw new Error('banco inválido');

  this.run = (0, _Utils.parseRun)(run);
  this.password = password;

  switch (bank) {
    case Flashlib.BANK.SANTANDER:
      this.account = new _Bank.Santander();
      break;
    default:
      throw new Error('banco inválido');
  }
};

Flashlib.BANK = _types.BANK;
Flashlib.ACCOUNT = _types.ACCOUNT;
exports.default = Flashlib;