'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _types = require('./types');

var _Utils = require('./Utils');

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Account = function Account(_ref) {
  var number = _ref.number,
      type = _ref.type,
      balance = _ref.balance,
      bank = _ref.bank,
      coin = _ref.coin,
      hash = _ref.hash;

  _classCallCheck(this, Account);

  if (!number || !(0, _Utils.parseNumber)(number)) throw new Error('numero de cuenta invalido');
  if (!type || !Object.values(_types.ACCOUNT).includes(type)) throw new Error('tipo de cuenta invalido');
  if (typeof balance !== 'number') throw new Error('balance invalido');
  if (!bank || !Object.values(_types.BANK).includes(bank)) throw new Error('banco inválido');
  if (!coin || !Object.values(_types.COIN).includes(coin)) throw new Error('moneda no soportada');

  this.number = (0, _Utils.parseNumber)(number);
  this.type = type;
  this.balance = balance;
  this.bank = bank;
  this.coin = coin;
  if (hash) this.hash = hash;
};

exports.default = Account;